#pragma once
#include "stdafx.h"
#include "Plugin.h"
#include "BehaviorTree.h"

#define M_PI_F 3.14159265358979323846f
#define M_PI_2_F 1.57079632679489661923f
#define M_2_PI_F 6.28318530718f

enum class Inventory : int {
	WEAPON = 0,
	FOOD = 1,
	MEDKIT = 2,
	FOODR = 3,
	OTHER = 4
};

struct Rect
{
	Rect() {};
	Rect(Elite::Vector2 topleft, Elite::Vector2 bottomright) :
		topLeft{ topleft }, bottomRight{ bottomright } {};

	Elite::Vector2 topLeft{};
	Elite::Vector2 bottomRight{};
};

bool PointInRectangle(Elite::Vector2 point, const Rect& rect)
{
	float minX{ 0 }, minY{ 0 }, maxX{ 0 }, maxY{ 0 };
	if (rect.bottomRight.x > rect.topLeft.x)
	{
		minX = rect.topLeft.x;
		maxX = rect.bottomRight.x;
	}
	else
	{
		maxX = rect.topLeft.x;
		minX = rect.bottomRight.x;
	}
	if (rect.bottomRight.y > rect.topLeft.y)
	{
		minY = rect.topLeft.y;
		maxY = rect.bottomRight.y;
	}
	else
	{
		maxY = rect.topLeft.y;
		minY = rect.bottomRight.y;
	}
	if (point.x <= maxX && point.x >= minX)
		if (point.y <= maxY && point.y >= minY)
			return true;

	return false;
}

//===========================================================================================
//HELPER FUNCTIONS
//===========================================================================================
EnemyInfo GetClosestEnemy(Plugin* plugin, IExamInterface* interface)
{
	EnemyInfo enemy{};
	enemy.Location = Elite::Vector2{ 90000.0f, 90000.0f };

	auto agentPos = interface->Agent_GetInfo().Position;

	for (auto entity : plugin->GetEntitiesInFOV())
	{
		if (entity.Type == eEntityType::ENEMY)
		{
			float disToCurrent = Elite::Distance(agentPos, enemy.Location);
			float disToNew = Elite::Distance(agentPos, entity.Location);
			if (disToCurrent > disToNew)
			{
				EnemyInfo temp{};
				interface->Enemy_GetInfo(entity, temp);
				enemy = temp;
				float disToEnemy = Elite::Distance(agentPos, temp.Location);
				if (disToEnemy < 2.0f)
				{
					enemy = temp;
				}
			}
		}
	}
	plugin->SetClosestEnemy(enemy.Location);
	return enemy;
}

EntityInfo GetClosestItem(Plugin* plugin, IExamInterface* interface)
{
	EntityInfo item{};
	item.Location = Elite::Vector2{ 90000.0f, 90000.0f };

	auto agentPos = interface->Agent_GetInfo().Position;

	for (auto entity : plugin->GetEntitiesInFOV())
	{
		if (entity.Type == eEntityType::ITEM)
		{
			float disToCurrent = Elite::Distance(agentPos, item.Location);
			float disToNew = Elite::Distance(agentPos, entity.Location);
			if (disToCurrent > disToNew)
			{
				item = entity;
			}
		}
	}
	return item;
}

HouseInfo GetClosestHouse(Plugin* plugin, IExamInterface* interface)
{
	HouseInfo tempHouse{};
	tempHouse.Center = { 90000.0f, 90000.0f };

	auto agentPos = interface->Agent_GetInfo().Position;

	if (plugin->GetHousesInFOV().size() > 1)
	{
		for (auto house : plugin->GetHousesInFOV())
		{
			if (!plugin->CheckPreviousHouses(house.Center))
			{
				tempHouse = house;
			}
			else
			{
				float disToCurrent = Elite::Distance(agentPos, tempHouse.Center);
				float disToNew = Elite::Distance(agentPos, house.Center);
				if (disToCurrent > disToNew)
				{
					tempHouse = house;
				}
			}
		}
	}
	else
	{
		if (plugin->GetHousesInFOV().size() != 0)
			tempHouse = plugin->GetHousesInFOV().at(0);
	}
	return tempHouse;
}

void FoodPickUp(IExamInterface* interface, Plugin* plugin, ItemInfo item)
{
	std::map<Inventory, int> map;
	map[Inventory::OTHER] = interface->Item_GetMetadata(item, "energy").iVal;
	bool hasFood = plugin->CheckInventorySlot(Inventory::FOOD);
	bool hasReserveFood = plugin->CheckInventorySlot(Inventory::FOODR);

	if (!hasFood)
	{
		//std::cout << "Added to first food slot\n";
		interface->Inventory_AddItem((UINT)Inventory::FOOD, item);
		plugin->SetInventorySlot(Inventory::FOOD, true);
	}
	else if (hasFood && !hasReserveFood)
	{
		//std::cout << "Added to second food slot\n";
		interface->Inventory_AddItem((UINT)Inventory::FOODR, item);
		plugin->SetInventorySlot(Inventory::FOODR, true);
	}
	else
	{
		//std::cout << "Checking food values\n";
		//std::cout << "Food value: " << map[Inventory::OTHER] << std::endl;
		//Add item in temp slot
		//interface->Inventory_AddItem((UINT)Inventory::OTHER, item);
		//Get all other food values
		ItemInfo temp{}, temp2{};
		interface->Inventory_GetItem((UINT)Inventory::FOOD, temp);
		map[Inventory::FOOD] = interface->Item_GetMetadata(temp, "energy").iVal;
		interface->Inventory_GetItem((UINT)Inventory::FOODR, temp2);
		map[Inventory::FOODR] = interface->Item_GetMetadata(temp2, "energy").iVal;

		//Checks for lowest food value and eat it
		Inventory smallestValue{ Inventory::FOOD };
		if (map[Inventory::FOOD] > map[Inventory::FOODR])
			smallestValue = Inventory::FOODR;

		if (map[smallestValue] < map[Inventory::OTHER])
		{
			//std::cout << "Using food item in valid slots\n";
			interface->Inventory_UseItem((UINT)smallestValue);
			interface->Inventory_RemoveItem((UINT)smallestValue);
			plugin->SetInventorySlot(smallestValue, false);
			interface->Inventory_AddItem((UINT)smallestValue, item);
			plugin->SetInventorySlot(smallestValue, true);
		}
		else
		{
			//std::cout << "Using food item from other slot\n";
			interface->Inventory_AddItem((UINT)Inventory::OTHER, item);
			interface->Inventory_UseItem((UINT)Inventory::OTHER);
			interface->Inventory_RemoveItem((UINT)Inventory::OTHER);
		}
	}

}

void MedkitPickUp(IExamInterface* interface, Plugin* plugin, ItemInfo item)
{
	bool hasMedkit = plugin->CheckInventorySlot(Inventory::MEDKIT);
	int healValue = interface->Item_GetMetadata(item, "health").iVal;

	if (!hasMedkit)
	{
		//std::cout << "Added medkit to inventory\n";
		interface->Inventory_AddItem((UINT)Inventory::MEDKIT, item);
	}
	else
	{
		//std::cout << "Checking medkit values\n";
		ItemInfo temp{};
		interface->Inventory_GetItem((UINT)Inventory::MEDKIT, temp);
		int currentHealth = interface->Item_GetMetadata(temp, "health").iVal;
		//std::cout << "Current medkit: " << currentHealth << " vs " << healValue << std::endl;
		if (currentHealth >= healValue)
		{
			//std::cout << "Removing medkit from other slot\n";
			interface->Inventory_AddItem((UINT)Inventory::OTHER, item);
			interface->Inventory_UseItem((UINT)Inventory::OTHER);
			interface->Inventory_RemoveItem((UINT)Inventory::OTHER);
		}
		else
		{
			//std::cout << "Swapping out medkits from slots\n";
			interface->Inventory_UseItem((UINT)Inventory::MEDKIT);
			interface->Inventory_RemoveItem((UINT)Inventory::MEDKIT);
			interface->Inventory_AddItem((UINT)Inventory::MEDKIT, item);
		}
	}
	plugin->SetInventorySlot(Inventory::MEDKIT, true);
}

void PistolPickUp(IExamInterface* interface, Plugin* plugin, ItemInfo item)
{
	bool hasGun = plugin->CheckInventorySlot(Inventory::WEAPON);
	int ammo = interface->Item_GetMetadata(item, "ammo").iVal;

	if (!hasGun)
	{
		//std::cout << "Added weapon to inventory\n";
		interface->Inventory_AddItem((UINT)Inventory::WEAPON, item);
	}
	else
	{
		//std::cout << "Checking ammo\n";
		ItemInfo temp{};
		interface->Inventory_GetItem((UINT)Inventory::WEAPON, temp);
		int currentAmmo = interface->Item_GetMetadata(temp, "ammo").iVal;
		//std::cout << "Current ammo: " << currentAmmo << " vs " << ammo << std::endl;
		if (currentAmmo >= ammo)
		{
			//std::cout << "Removing weapon from other slot\n";
			interface->Inventory_AddItem((UINT)Inventory::OTHER, item);
			interface->Inventory_RemoveItem((UINT)Inventory::OTHER);
		}
		else
		{
			//std::cout << "Swapping out weapons from slots\n";
			interface->Inventory_RemoveItem((UINT)Inventory::WEAPON);
			interface->Inventory_AddItem((UINT)Inventory::WEAPON, item);
		}
	}
	plugin->SetInventorySlot(Inventory::WEAPON, true);
}

void GarbagePickUp(IExamInterface* interface, ItemInfo item)
{
	interface->Inventory_AddItem((UINT)Inventory::OTHER, item);
	interface->Inventory_RemoveItem((UINT)Inventory::OTHER);
}

//===========================================================================================
//Behavior tree functions
//===========================================================================================
//-------------------------------------------------------------------------------------------
//Conditionals
//-------------------------------------------------------------------------------------------
bool EnemyInFOV(Blackboard* bb)
{
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface);
	if (!result)
		return false;

	auto enemy = GetClosestEnemy(plugin, interface);
	if (enemy.EnemyHash)
	{
		plugin->SetAutoOrient(false);
		return true;
	}

	plugin->SetAutoOrient(true);
	return false;
}

bool HasGun(Blackboard* bb)
{
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface);
	if (!result)
		return false;

	if (plugin->CheckInventorySlot(Inventory::WEAPON))
	{
		return true;
	}

	return false;
}

bool ItemInFOV(Blackboard* bb)
{
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface);
	if (!result)
		return false;

	auto item = GetClosestItem(plugin, interface);
	if (item.EntityHash)
	{
		return true;
	}

	return false;
}

bool LowHealth(Blackboard* bb)
{
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("interface", interface);
	if (!result)
		return false;

	auto agent = interface->Agent_GetInfo();
	return (agent.Health <= 2);
}

bool HasMedkit(Blackboard* bb)
{
	Plugin* plugin = nullptr;
	auto result = bb->GetData("plugin", plugin);
	if (!result)
		return false;

	return plugin->CheckInventorySlot(Inventory::MEDKIT);
}

bool LowEnergy(Blackboard* bb)
{
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("interface", interface);
	if (!result)
		return false;

	auto agent = interface->Agent_GetInfo();
	return (agent.Energy <= 2);
}

bool HasFood(Blackboard* bb)
{
	Plugin* plugin = nullptr;
	auto result = bb->GetData("plugin", plugin);
	if (!result)
		return false;

	return (plugin->CheckInventorySlot(Inventory::FOOD) || plugin->CheckInventorySlot(Inventory::FOODR));
}

bool HouseInFOV(Blackboard* bb) 
{
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface);
	if (!result)
		return Failure;

	auto house = GetClosestHouse(plugin, interface);

	return (plugin->GetHousesInFOV().size() > 0 && !plugin->CheckPreviousHouses(house.Center));
}

bool GoingToHouse(Blackboard* bb)
{
	Plugin* plugin = nullptr;
	auto result = bb->GetData("plugin", plugin);
	if (!result)
		return false;

	return plugin->GetIsGoingToHouse();
}

bool IsCollecting(Blackboard* bb)
{
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface);
	if (!result)
		return false;

	auto item = GetClosestItem(plugin, interface);

	if (plugin->GetIsCollectingItem() == false)
	{
		//std::cout << "Colelcting\n";
		plugin->SetIsCollectingItem(true);
		plugin->AddToPriorityQueue(item.Location);
	}
	else
		return true;

	return false;
}

bool CloseToTarget(Blackboard* bb)
{
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface);
	if (!result)
		return false;

	auto agent = interface->Agent_GetInfo();
	auto target = plugin->GetTarget();
	if (Elite::Distance(agent.Position, target) <= 3.0f)
	{
		//std::cout << "Close to Target\n";
		return true;
	}

	return false;
}

bool CorrectTarget(Blackboard* bb)
{
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface);
	if (!result)
		return false;

	if (plugin->CheckIfCorrectTarget())
	{
		std::cout << "Correct Target\n";
	}

	return plugin->CheckIfCorrectTarget();
	
}

//-------------------------------------------------------------------------------------------
//Actions
//-------------------------------------------------------------------------------------------
BehaviorState Aim(Blackboard* bb)
{
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	SteeringPlugin_Output* steering = {};
	auto result = bb->GetData("plugin", plugin) 
				&& bb->GetData("interface", interface)
				&& bb->GetData("steering", steering);
	if (!result)
		return Failure;

	auto enemy = GetClosestEnemy(plugin, interface);
	auto agent = interface->Agent_GetInfo();

	Elite::Vector2 newVector = Elite::Vector2{ enemy.Location - agent.Position };
	float angle = atan2(newVector.y, newVector.x);
	float orient = agent.Orientation;

	//atan returns value between [-pi, pi] 
	//orient can have bigger or smaller values than that
	//thus we adjust them accordingly using a full circle
	while (orient > M_PI_F) { orient -= M_2_PI_F; }
	while (orient < -M_PI_F) { orient += M_2_PI_F; }

	if (angle > orient - M_PI_2_F + 0.05f)
	{
		steering->AngularVelocity = 100.0f;
		return Failure;
	}
	else if (angle < orient - M_PI_2_F - 0.05f)
	{
		steering->AngularVelocity = -100.0f;
		return Failure;
	}
	else if (angle > orient - M_PI_2_F)
	{
		steering->AngularVelocity = 0.1f;
		return Success;
	}
	else if (angle < orient - M_PI_2_F)
	{
		steering->AngularVelocity = -0.1f;
		return Success;
	}

	return Failure;
}

BehaviorState Shoot(Blackboard* bb)
{
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface);
	if (!result)
		return Failure;

	ItemInfo weapon{};
	interface->Inventory_GetItem((UINT)Inventory::WEAPON, weapon);
	if (interface->Item_GetMetadata(weapon, "ammo").iVal > 0)
		interface->Inventory_UseItem((UINT)Inventory::WEAPON);
	else
	{
		interface->Inventory_RemoveItem((UINT)Inventory::WEAPON);
		plugin->SetInventorySlot(Inventory::WEAPON, false);
		return Failure;
	}

	return Success;
}

BehaviorState Avoid(Blackboard* bb)
{
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	SteeringPlugin_Output* steering = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface) && bb->GetData("steering", steering);
	if (!result)
		return Failure;

	auto agentInfo = interface->Agent_GetInfo();
	plugin->SetCanRun(true);

	float maxSeeAhead = 8.0f;
	auto ahead = agentInfo.Position + agentInfo.LinearVelocity * maxSeeAhead;
	auto ahead2 = agentInfo.Position + agentInfo.LinearVelocity * maxSeeAhead * 0.5f;
	
	Elite::Vector2 avoidanceVelocity{};
	auto closestEnemy = GetClosestEnemy(plugin, interface);
	if (closestEnemy.EnemyHash)
	{
		auto distance = Elite::Distance(closestEnemy.Location, ahead);
		auto distance2 = Elite::Distance(closestEnemy.Location, ahead2);
		if (distance <= 2.0f || distance2 <= 2.0f)
		{
			avoidanceVelocity = ahead - closestEnemy.Location;
			avoidanceVelocity.Normalize();
			avoidanceVelocity *= 10.0f;
		}
	}
	else
	{
		plugin->SetAutoOrient(true);
		return Failure;
	}

	plugin->SetAutoOrient(false);
	steering->LinearVelocity += avoidanceVelocity;

	return Success;
}

BehaviorState Seek(Blackboard* bb)
{
	SteeringPlugin_Output* steering = nullptr;
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface) && bb->GetData("steering", steering);
	if (!result)
		return Failure;

	auto itemPos = interface->NavMesh_GetClosestPathPoint(plugin->GetTarget());
	auto agent = interface->Agent_GetInfo();

	steering->LinearVelocity = itemPos - agent.Position;
	steering->LinearVelocity.Normalize();
	steering->LinearVelocity *= agent.MaxLinearSpeed;

	return Success;
}

BehaviorState PickUpItem(Blackboard* bb)
{
	//std::cout << "PickUpItem\n";
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface);
	if (!result)
		return Failure;

	auto entity = GetClosestItem(plugin, interface);
	ItemInfo item{};
	interface->Item_Grab(entity, item);

	switch (item.Type)
	{
	case eItemType::FOOD:
		//std::cout << "Found food item\n";
		FoodPickUp(interface, plugin, item);
		break;
	case eItemType::MEDKIT:
		//std::cout << "Found medkit item\n";
		MedkitPickUp(interface, plugin, item);
		break;
	case eItemType::PISTOL:
		//std::cout << "Found pistol\n";
		PistolPickUp(interface, plugin, item);
		break;
	case eItemType::GARBAGE:
		//std::cout << "Found fekking garbage??\n";
		GarbagePickUp(interface, item);
		break;
	default:
		break;
	}

	plugin->SetIsCollectingItem(false);
	return Success;
}

BehaviorState Heal(Blackboard* bb)
{
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface);
	if (!result)
		return Failure;

	interface->Inventory_UseItem((UINT)Inventory::MEDKIT);
	interface->Inventory_RemoveItem((UINT)Inventory::MEDKIT);
	plugin->SetInventorySlot(Inventory::MEDKIT, false);

	return Success;
}

BehaviorState Eat(Blackboard* bb)
{
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface);
	if (!result)
		return Failure;

	if (plugin->CheckInventorySlot(Inventory::FOODR))
	{
		interface->Inventory_UseItem((UINT)Inventory::FOODR);
		interface->Inventory_RemoveItem((UINT)Inventory::FOODR);
		plugin->SetInventorySlot(Inventory::FOODR, false);
	}
	else
	{
		interface->Inventory_UseItem((UINT)Inventory::FOOD);
		interface->Inventory_RemoveItem((UINT)Inventory::FOOD);
		plugin->SetInventorySlot(Inventory::FOOD, false);
	}

	return Success;
}

BehaviorState HouseCalculations(Blackboard* bb)
{
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface);
	if (!result)
		return Failure;
	auto house = GetClosestHouse(plugin, interface);
	plugin->DoHouseCalculations(house);
	plugin->SetIsGoingToHouse(true);
	return Success;
}

BehaviorState SetNewTarget(Blackboard* bb)
{
	std::cout << "Set New Target\n";
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface);
	if (!result)
		return Failure;

	plugin->SetTarget(plugin->CalculateNextTarget());
	return Success;
}

BehaviorState GetNewTarget(Blackboard* bb)
{
	std::cout << "Get New Target\n";
	Plugin* plugin = nullptr;
	IExamInterface* interface = nullptr;
	auto result = bb->GetData("plugin", plugin) && bb->GetData("interface", interface);
	if (!result)
		return Failure;

	plugin->CheckIfCorrectTarget();
	plugin->SetTarget(plugin->CalculateNextTarget());
	return Success;
}