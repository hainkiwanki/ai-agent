#pragma once
#include "IExamPlugin.h"
#include "Exam_HelperStructs.h"
#include "BehaviorTree.h"

enum class Inventory;
struct Rect;
class IBaseInterface;
class IExamInterface;

class Plugin :public IExamPlugin
{
public:
	Plugin() {};
	virtual ~Plugin() {};

	void Initialize(IBaseInterface* pInterface, PluginInfo& info) override;
	void DllInit() override;
	void DllShutdown() override;

	void InitGameDebugParams(GameDebugParams& params) override;
	void ProcessEvents(const SDL_Event& e) override;

	SteeringPlugin_Output UpdateSteering(float dt) override;
	void Render(float dt) const override;

	bool GetCanRun() { return m_CanRun; };
	void SetCanRun(bool b) { m_CanRun = b; };
	void SetAutoOrient(bool b) { m_IsAutoOriented = b; };
	bool GetIsDoneExploring() { return m_IsDoneExploring; };
	void SetIsDoneExploring(bool b) { m_IsDoneExploring = b; };;
	bool CheckInventorySlot(Inventory slot) { return m_Inventory[slot]; };
	void SetInventorySlot(Inventory slot, bool state) { m_Inventory[slot] = state; };
	void SetClosestEnemy(Elite::Vector2 pos) { m_ClosestEnemy = pos; };
	void SetTarget(Elite::Vector2 pos) { m_Target = pos; };
	Elite::Vector2 GetTarget() { return m_Target; };
	Elite::Vector2 CalculateNextTarget();
	void SetIsCollectingItem(bool b) { m_IsCollectingItem = b; };
	bool GetIsCollectingItem() { return m_IsCollectingItem; };
	void SetIsGoingToHouse(bool b) { m_IsGoingToHouse = b; };
	bool GetIsGoingToHouse() { return m_IsGoingToHouse; };


	vector<HouseInfo> GetHousesInFOV() const;
	vector<EntityInfo> GetEntitiesInFOV() const;


	bool CheckPreviousHouses(const Elite::Vector2& center);
	void DoHouseCalculations(const HouseInfo& house);
	bool CheckIfCorrectTarget();
	void AddToPriorityQueue(const Elite::Vector2 target);

private:
	BehaviorTree * m_pBehaviorTree = nullptr;
	IExamInterface* m_pInterface = nullptr;
	SteeringPlugin_Output m_Steering = {};

	int m_AmtOfCubes = 6;
	int m_CurrentTarget = 0;
	int m_HouseCounter = 0;
	float m_Timer = 0;
	float m_AngSpeed = 0.f;
	bool doItOnce = false;
	bool m_IsAutoOriented = true;
	bool m_CanRun = false;
	bool m_IsDoneExploring = false;
	bool m_IsDoneWithFinalPoints = false;
	bool m_IsCollectingItem = false;
	bool m_IsGoingToHouse = false;
	bool m_PrvInHouse = false;

	Elite::Vector2 m_ClosestEnemy = {};
	Elite::Vector2 m_Target = {};
	std::vector<Elite::Vector2> m_NavPoints{};
	std::vector<Elite::Vector2> m_RayPoints{};
	std::vector<Elite::Vector2> m_FinalPoints{};
	std::vector<Elite::Vector3> m_Color{};
	std::vector<bool> m_RectWeights;
	std::vector<bool> m_GoToFinalPoint;
	std::vector<Rect> m_Rectangles{};
	std::map<int, Rect> m_HouseMap{};
	std::map<Inventory, bool> m_Inventory;
	std::deque<Elite::Vector2> m_PriorityDeque{};
	std::deque<Elite::Vector2> m_PreviousHouses{};

	void MakeRectangles(int rows, int columns);
	void CalculateSomePoints();
	const Rect& AddHouseToMap(const HouseInfo& house);
	void DisableObseletePoints(const Rect& rect);
	bool CheckTargets(Elite::Vector2 tar1, Elite::Vector2 tar2);
	void CheckFinalPoint();
};

//ENTRY
//This is the first function that is called by the host program
//The plugin returned by this function is also the plugin used by the host program
extern "C"
{
	__declspec (dllexport) IPluginBase* Register()
	{
		return new Plugin();
	}
}