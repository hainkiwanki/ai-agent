#include "stdafx.h"
#include "Plugin.h"
#include "IExamInterface.h"
#include "Functions.h"

void Plugin::Initialize(IBaseInterface* pInterface, PluginInfo& info)
{
	m_pInterface = static_cast<IExamInterface*>(pInterface);

	info.BotName = "BottestBot";
	info.Student_FirstName = "Kevin";
	info.Student_LastName = "Radino";
	info.Student_Class = "2DAE1";

	auto pBB = new Blackboard();
	pBB->AddData("interface", m_pInterface);
	pBB->AddData("plugin", this);
	pBB->AddData("steering", &m_Steering);

	m_Inventory[Inventory::FOOD] = false;
	m_Inventory[Inventory::FOODR] = false;
	m_Inventory[Inventory::MEDKIT] = false;
	m_Inventory[Inventory::WEAPON] = false;
	m_Inventory[Inventory::OTHER] = false;

	m_pBehaviorTree = new BehaviorTree(
		pBB,
		new BehaviorSelector({
			new BehaviorSequence({
				new BehaviorConditional(LowHealth),
				new BehaviorConditional(HasMedkit),
				new BehaviorAction(Heal)
			}),
			new BehaviorSequence({
				new BehaviorConditional(LowEnergy),
				new BehaviorConditional(HasFood),
				new BehaviorAction(Eat)
			}),
			new BehaviorSequence({
				new BehaviorConditional(EnemyInFOV),
				new BehaviorSelector({
					new BehaviorSequence({
						new BehaviorConditional(HasGun),
						new BehaviorAction(Aim),
						new BehaviorAction(Shoot)
					}),
					new BehaviorAction(Avoid)
				})
			}),
			new BehaviorSequence({
				new BehaviorConditional(HouseInFOV),
				new BehaviorUnConditional(GoingToHouse),
				new BehaviorAction(HouseCalculations),
				new BehaviorAction(SetNewTarget)
			}),
			new BehaviorSequence({
				new BehaviorConditional(ItemInFOV),
				new BehaviorUnConditional(IsCollecting),
				new BehaviorAction(SetNewTarget)
			}),
			new BehaviorSequence({
				new BehaviorConditional(CloseToTarget),
				new BehaviorSelector({
					new BehaviorSequence({
						new BehaviorConditional(ItemInFOV),
						new BehaviorAction(PickUpItem)
					}),
					new BehaviorSequence({
						new BehaviorConditional(CorrectTarget),
						new BehaviorAction(GetNewTarget)
					}),
					new BehaviorAction(SetNewTarget)
				})
			}),
			new BehaviorAction(Seek)
		}));
}

void Plugin::DllInit()
{
	//_CrtSetBreakAlloc(1311);
}

void Plugin::DllShutdown()
{
	SAFE_DELETE(m_pBehaviorTree);
}

void Plugin::InitGameDebugParams(GameDebugParams& params)
{
	params.AutoFollowCam = true;
	params.RenderUI = true;
	params.SpawnEnemies = true;
	params.EnemyCount = 20;
	params.GodMode = false;
	//params.LevelFile = "LevelTwo.gppl";
	params.AutoGrabClosestItem = true;
	params.OverrideDifficulty = false;
	params.Difficulty = 1.f;
}

void Plugin::ProcessEvents(const SDL_Event& e)
{}

SteeringPlugin_Output Plugin::UpdateSteering(float dt)
{
	if(doItOnce == false)
		CalculateSomePoints();

	m_Steering.AutoOrientate = m_IsAutoOriented;
	m_Steering.RunMode = m_CanRun;

	if (m_CanRun)
	{
		m_Timer += dt;
	}
	if (m_Timer >= 3.0f)
	{
		m_Timer = 0.0f;
		m_CanRun = false;
	}

	auto agentPos = m_pInterface->Agent_GetInfo().Position;
	for (int i = 0; i < (int)m_Rectangles.size(); i++)
	{
		if (!m_RectWeights[i])
		{
			if (PointInRectangle(agentPos, m_Rectangles.at(i)))
				m_RectWeights[i] = true;
		}
	}

	auto agentInHouse = m_pInterface->Agent_GetInfo().IsInHouse;
	if (m_IsDoneWithFinalPoints)
	{
		if (m_PrvInHouse && !agentInHouse)
		{
			m_IsGoingToHouse = false;
		}
		m_PrvInHouse = agentInHouse;
	}

	m_pBehaviorTree->GetBlackboard()->ChangeData("steering", &m_Steering);
	m_pBehaviorTree->Update();

	return m_Steering;
}

void Plugin::Render(float dt) const
{
	auto agentInfo = m_pInterface->Agent_GetInfo();

	//Misc
	m_pInterface->Draw_SolidCircle(m_Target, 2.f, { 0,0 }, { 1, 0, 0 });
	//m_pInterface->Draw_Circle(m_ClosestEnemy, 2.0f, { 0,1,1 });

	//Forward vector
	//Elite::Vector2 forw{};
	//forw.x = agentInfo.Position.x + (10 * cos(agentInfo.Orientation - (float)M_PI / 2.0f));
	//forw.y = agentInfo.Position.y + (10 * sin(agentInfo.Orientation - (float)M_PI / 2.0f));
	//m_pInterface->Draw_Segment(agentInfo.Position, forw, { 1,0,0 });

	//Perpendicular to forward vector
	//Elite::Vector2 perp{};
	//perp.x = agentInfo.Position.x + (10 * cos(agentInfo.Orientation - 2*(float)M_PI / 2.0f));
	//perp.y = agentInfo.Position.y + (10 * sin(agentInfo.Orientation - 2*(float)M_PI / 2.0f));
	//m_pInterface->Draw_Segment(agentInfo.Position, perp, { 1,0,1 });

	//Draw world border
	//auto worldinfo = m_pInterface->World_GetInfo();
	//float width = worldinfo.Dimensions.x / 2.0f;
	//float height = worldinfo.Dimensions.y / 2.0f;
	//vector<Elite::Vector2> worldBorder{};
	//worldBorder.push_back(Elite::Vector2{worldinfo.Center.x + width, worldinfo.Center.y + height });
	//worldBorder.push_back(Elite::Vector2{worldinfo.Center.x + width, worldinfo.Center.y - height });
	//worldBorder.push_back(Elite::Vector2{worldinfo.Center.x - width, worldinfo.Center.y - height });
	//worldBorder.push_back(Elite::Vector2{worldinfo.Center.x - width, worldinfo.Center.y + height });
	//m_pInterface->Draw_Polygon(&worldBorder.at(0), 4, { 1,0,0 });

	//Draw raycast + points
	//for (int o = 0; o < (int)m_NavPoints.size(); o++)
	//{
	//	m_pInterface->Draw_Point(m_NavPoints.at(o), 5, m_Color.at(o));
	//	m_pInterface->Draw_Segment(agentInfo.Position, m_RayPoints.at(o), m_Color.at(o));
	//}

	//Draw final points
	//for (auto p : m_FinalPoints)
	//{
	//	m_pInterface->Draw_Point(p, 1, { 1,1,1 });
	//}

	//Draw cubes
	//float cubeWidth = width * 2.0f / m_AmtOfCubes;
	//float cubeHeight = height * 2.0f / m_AmtOfCubes;
	//for (int i = 0; i < (int)m_Rectangles.size(); i++)
	//{
	//	std::vector<Elite::Vector2> points{};
	//	points.push_back(m_Rectangles[i].topLeft);
	//	points.push_back(m_Rectangles[i].topLeft + Elite::Vector2{ cubeWidth, 0.0f });
	//	points.push_back(m_Rectangles[i].bottomRight);
	//	points.push_back(m_Rectangles[i].bottomRight + Elite::Vector2{ -cubeWidth, 0.0f });
	//	m_pInterface->Draw_Polygon(&points.at(0), 4, { 1,1,1 });

	//	Elite::Vector2 center = m_Rectangles[i].topLeft - Elite::Vector2(-cubeWidth / 2.0f, cubeHeight / 2.0f);

	//	if (m_RectWeights.at(i))
	//		m_pInterface->Draw_Circle(center, cubeHeight / 2.0f, /*{ 0,0 },*/ { 0,1,0 });
	//}

	//auto point = agentInfo.Position + agentInfo.LinearVelocity * 2.0f;
	//m_pInterface->Draw_Segment(agentInfo.Position, point, { 0,1,0 });

	//Draw house cubes
	for (auto h : m_HouseMap)
	{
		std::vector<Elite::Vector2> points{};
		auto width = abs(h.second.bottomRight.x - h.second.topLeft.x);
		auto height = abs(h.second.topLeft.x - h.second.bottomRight.x);
		points.push_back(h.second.topLeft);
		points.push_back(h.second.topLeft + Elite::Vector2{ width, 0.0f });
		points.push_back(h.second.bottomRight);
		points.push_back(h.second.bottomRight + Elite::Vector2{ -height, 0.0f });
		m_pInterface->Draw_Polygon(&points.at(0), 4, { 1,0,1 });
	}
}

Elite::Vector2 Plugin::CalculateNextTarget()
{
	if (m_PriorityDeque.size() > 0)
	{
		auto pos = m_PriorityDeque.at(0);
		return m_PriorityDeque.at(0);
	}
	if (!m_IsDoneWithFinalPoints) //send to next point
	{
		return m_FinalPoints[m_CurrentTarget];
	}
	else if (m_IsDoneWithFinalPoints && !m_IsDoneExploring) //send to next undiscovered square
	{
		Elite::Vector2 center{};
		if (!PointInRectangle(m_Target, m_Rectangles[0]))
			center = (m_Rectangles[0].topLeft + m_Rectangles[0].bottomRight) / 2.0f;
		else
			center = Elite::Vector2{ 90000.0f, 90000.0f };
		Elite::Vector2 closest = center;
		Elite::Vector2 agentPos = m_pInterface->Agent_GetInfo().Position;
		for (int i = 0; i < (int)m_Rectangles.size(); i++)
		{
			if (!m_RectWeights[i])
			{
				std::cout << i << ", ";
				auto newcenter = (m_Rectangles[i].topLeft + m_Rectangles[i].bottomRight) / 2.0f;
				float distance = Elite::Distance(closest, agentPos);
				auto newDistance = Elite::Distance(newcenter, agentPos);
				if (newDistance < distance)
				{
					closest = newcenter;
				}
			}
		}

		return closest;

	}
	else //send to random house
	{
		int random = rand() % (int)m_HouseMap.size();
		auto house = m_HouseMap.at(random);
		Elite::Vector2 center = (house.topLeft + house.bottomRight) / 2.0f;

		return center;
	}
	return Elite::Vector2();
}

vector<HouseInfo> Plugin::GetHousesInFOV() const
{
	vector<HouseInfo> vHousesInFOV = {};

	HouseInfo hi = {};
	for (int i = 0;; ++i)
	{
		if (m_pInterface->Fov_GetHouseByIndex(i, hi))
		{
			vHousesInFOV.push_back(hi);
			continue;
		}

		break;
	}

	return vHousesInFOV;
}

vector<EntityInfo> Plugin::GetEntitiesInFOV() const
{
	vector<EntityInfo> vEntitiesInFOV = {};

	EntityInfo ei = {};
	for (int i = 0;; ++i)
	{
		if (m_pInterface->Fov_GetEntityByIndex(i, ei))
		{
			vEntitiesInFOV.push_back(ei);
			continue;
		}

		break;
	}

	return vEntitiesInFOV;
}

void Plugin::CalculateSomePoints()
{
	MakeRectangles(m_AmtOfCubes, m_AmtOfCubes);
	auto agentInfo = m_pInterface->Agent_GetInfo();
	auto worldinfo = m_pInterface->World_GetInfo();
	int range = 250;
	int amtOfPoints = 32;
	auto angle = M_2_PI_F / amtOfPoints;
	std::map<int, bool> pointsDone{};

	for (int j = 20; j < range; j+= 10)
	{
		for (int i = 0; i < amtOfPoints; i++)
		{
			if (!pointsDone[i])
			{
				Elite::Vector2 temp{0,0};
				temp.x = agentInfo.Position.x + j * cos(angle * i);
				temp.y = agentInfo.Position.y + j * sin(angle * i);
				
				for (int k = 0; k < (int)m_Rectangles.size(); k++)
				{
					if (PointInRectangle(temp, m_Rectangles.at(k)))
					{
						m_RectWeights.at(k) = true;
						break;
					}
				}
				auto navPoint = m_pInterface->NavMesh_GetClosestPathPoint(temp);
				if ((int)navPoint.x != (int)temp.x &&
					(int)navPoint.y != (int)temp.y)
				{
					m_NavPoints.push_back(navPoint);
					m_RayPoints.push_back(temp);
					float colorR = (rand() % 1000) / 1000.f;
					float colorG = (rand() % 1000) / 1000.0f;
					float colorB = (rand() % 1000) / 1000.0f;
					m_Color.push_back(Elite::Vector3{ colorR, colorG, colorB });
					pointsDone[i] = true;
				}
			}
		}
	}

	for (int i = 0; i < (int)m_NavPoints.size(); i++)
	{
		if (Elite::Distance(m_NavPoints[i], m_RayPoints[i]) >= 20.0f)
		{
			m_FinalPoints.push_back(m_RayPoints[i]);
			m_GoToFinalPoint.push_back(true);
		}

		m_FinalPoints.push_back(m_NavPoints[i]);
		m_GoToFinalPoint.push_back(true);
	}

	doItOnce = true;
	m_Target = m_FinalPoints.at(0);
	m_GoToFinalPoint.at(0) = false;
}

bool Plugin::CheckPreviousHouses(const Elite::Vector2 & center)
{
	for (auto hCenter : m_PreviousHouses)
	{
		if (CheckTargets(hCenter, center))
			return true;
	}
	return false;
}

void Plugin::DoHouseCalculations(const HouseInfo & house)
{
	if (!m_IsDoneExploring)
	{
		auto houseRect = AddHouseToMap(house);
		if (!m_IsDoneWithFinalPoints)
		{
			DisableObseletePoints(houseRect);
			CheckFinalPoint();
		}
	}
	AddToPriorityQueue(house.Center);
	if (m_PreviousHouses.size() >= 2)
	{
		m_PreviousHouses.pop_back();
	}
	m_PreviousHouses.push_front(house.Center);
}

bool Plugin::CheckIfCorrectTarget()
{
	if (m_PriorityDeque.size() > 0)
	{
		if (CheckTargets(m_PriorityDeque.at(0), m_Target))
		{
			m_PriorityDeque.pop_front();
			return true;
		}
	}
	else if(!m_IsDoneWithFinalPoints)
	{
		if (CheckTargets(m_FinalPoints[m_CurrentTarget], m_Target))
		{
			m_IsGoingToHouse = false;
			m_CurrentTarget++;
			if (m_CurrentTarget >= (int)m_GoToFinalPoint.size())
			{
				std::cout << "Done with final points\n";
				m_IsDoneWithFinalPoints = true;
			}
			else
			{
				for (int i = m_CurrentTarget; i < (int)m_FinalPoints.size(); i++)
				{
					if (m_GoToFinalPoint[i])
					{
						m_CurrentTarget = i;
						break;
					}
				}
			}
			return true;
		}
	}
	else if (m_IsDoneWithFinalPoints && !m_IsDoneExploring)
	{
		bool allRectsDone = true;
		for (int i = 0; i < (int)m_Rectangles.size(); i++)
		{
			if (!m_RectWeights[i])
			{
				allRectsDone = false;
				if (PointInRectangle(m_Target, m_Rectangles[i]))
				{
					m_RectWeights[i] = true;
					return true;
					//break;
				}
			}
			else continue;
		}
		if (allRectsDone)
		{
			std::cout << "No More Rects To Do\n";
			m_IsDoneExploring = true;
		}
	}
	
	return false;
}

void Plugin::AddToPriorityQueue(const Elite::Vector2 target)
{
	m_PriorityDeque.push_front(target);
}

void Plugin::MakeRectangles(int rows, int columns)
{
	auto worldinfo = m_pInterface->World_GetInfo();
	auto cubeWidth = worldinfo.Dimensions.x / columns;
	auto cubeHeight = worldinfo.Dimensions.y / rows;
	Elite::Vector2 topLeftPoint = Elite::Vector2{ worldinfo.Center.x - worldinfo.Dimensions.x / 2.0f, worldinfo.Center.y + worldinfo.Dimensions.y / 2.0f};

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			Rect temp{};
			temp.topLeft = Elite::Vector2{ topLeftPoint.x + j * cubeWidth, topLeftPoint.y - i * cubeHeight};
			temp.bottomRight = temp.topLeft - Elite::Vector2{-cubeWidth, cubeHeight};
			m_Rectangles.push_back(temp);
			m_RectWeights.push_back(false);
		}
	}
}

const Rect& Plugin::AddHouseToMap(const HouseInfo & house)
{
	Rect houseRect{};
	houseRect.topLeft = house.Center + Elite::Vector2{ -house.Size.x / 2.0f, -house.Size.y / 2.0f };
	houseRect.bottomRight = house.Center + Elite::Vector2{ house.Size.x / 2.0f, house.Size.y / 2.0f };
	houseRect.topLeft += Elite::Vector2{ -2.5f, -2.5f };
	houseRect.bottomRight += Elite::Vector2{ 2.5f, 2.5f };
	if (m_HouseMap.size() == 0)
	{
		m_HouseMap[m_HouseCounter] = houseRect;
	}
	else
	{
		for (auto h : m_HouseMap)
		{
			if (PointInRectangle(house.Center, h.second))
			{
				//std::cout << "Already in map\n";
				return houseRect;
			}
		}
		m_HouseMap[m_HouseCounter] = houseRect;
	}
	m_HouseCounter++;

	return houseRect;
}

void Plugin::DisableObseletePoints(const Rect& rect)
{
	for (int i = 0; i < (int)m_FinalPoints.size(); i++)
	{
		if (PointInRectangle(m_FinalPoints.at(i), rect))
		{
			m_GoToFinalPoint.at(i) = false;
		}
	}
}

bool Plugin::CheckTargets(Elite::Vector2 tar1, Elite::Vector2 tar2)
{
	if ((int)tar1.x == (int)tar2.x)
		if ((int)tar1.y == (int)tar2.y)
			return true;

	return false;
}

void Plugin::CheckFinalPoint()
{
	int houseIdx = m_HouseCounter - 1;
	auto currentHouse = m_HouseMap[houseIdx];
	Elite::Vector2 center = (currentHouse.topLeft + currentHouse.bottomRight) / 2.0f;

	Rect fitRect = currentHouse;
	fitRect.topLeft += Elite::Vector2{ 2.f, 2.f };
	fitRect.bottomRight += Elite::Vector2{ -2.f, -2.f };

	Rect smallRect = fitRect;
	smallRect.topLeft += Elite::Vector2{ 3.5f, 3.5f };
	smallRect.bottomRight += Elite::Vector2{ -3.5f, -3.5f };

	Elite::Vector2 point = m_FinalPoints.at(m_CurrentTarget);
	if (PointInRectangle(point, fitRect) && !PointInRectangle(point, smallRect))
	{
		std::cout << "Point in wall\n";
		float offset = 4.f;
		if (point.x >= center.x)
		{
			point.x -= offset;
		}
		else
		{
			point.x += offset;
		}
		if (point.y >= center.y)
		{
			point.y -= offset;
		}
		else
		{
			point.y += offset;
		}
		m_FinalPoints.at(m_CurrentTarget) = point;
	}
	
}
